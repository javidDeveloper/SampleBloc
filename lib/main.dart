import 'dart:async';

import 'package:blocsample/blocs/counter_bloc.dart';
import 'package:blocsample/screens/bloc_counter_screen.dart';
import 'package:blocsample/widget/bloc_provider.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Bloc Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider(bloc: CounterBloc(), child: BlocCounterScreen()),
    );
  }
}