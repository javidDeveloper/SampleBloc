import 'package:blocsample/blocs/counter_bloc.dart';
import 'package:blocsample/widget/bloc_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BlocCounterScreen extends StatefulWidget {

  BlocCounterScreen({Key key,}) : super(key: key);

  @override
  _BlocCounterState createState() => _BlocCounterState();
}

class _BlocCounterState extends State<BlocCounterScreen> {

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final CounterBloc counterBloc = BlocProvider.of<CounterBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("flutter bloc sample"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            StreamBuilder<int>(
              initialData: 0,
              stream: counterBloc.outCounter,
              builder: (context, snapshot) {
                return Text("${snapshot.data}",
                    style: Theme
                        .of(context)
                        .textTheme
                        .headline4);
              },
            ),
            Wrap(
              direction: Axis.horizontal,
              spacing: 5,
              children: [
                RaisedButton(child: Icon(Icons.minimize), onPressed:(){
                  counterBloc.decrement();
                },),
                RaisedButton(child: Icon(Icons.add), onPressed:(){
                  counterBloc.increment();
                },),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
