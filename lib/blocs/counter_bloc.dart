import 'dart:async';

import 'package:blocsample/widget/bloc_provider.dart';

class CounterBloc implements BaseBloc {
  int counter;

  StreamController<int> _counterController = StreamController();

  Sink<int> get _inAdd => _counterController.sink;

  Stream<int> get outCounter => _counterController.stream;

  CounterBloc() {
    counter = 0;
  }

  @override
  void dispose() {
    _counterController.close();
  }

  void increment() {
    counter++;
    _inAdd.add(counter);
  }
  void decrement() {
    counter--;
    _inAdd.add(counter);
  }
}
